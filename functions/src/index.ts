import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
// import * as firebaseHelper from 'firebase-functions-helper';
import * as express from 'express';
import * as bodyParser from "body-parser";

admin.initializeApp(functions.config().firebase);
const db = admin.firestore();

const app = express();
const main = express();

main.use('/v1/', app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));
// webApi is your functions name, and you will pass main as 
// a parameter
export const api = functions.region("asia-northeast1").https.onRequest(main);

app.post("/updatecry", async (req, res) => {
    try {
        const pokemonCollection = db.collection('pokemon').orderBy("entry", "desc");
        await pokemonCollection.get().then((querySnap) => {
            querySnap.docs.forEach(async (pokemon) => {
                await pokemon.ref.update({
                    "cry": `https://play.pokemonshowdown.com/audio/cries/${pokemon.data().name.toLowerCase()}.mp3`
                })
            })
            // querySnap
        })

        res.json({
            "result": "success"
        })

    } catch {
        res.status(400).send({
            error: "Failed getting list of Pokemon"
        })
    }

})

function _getColor(pokemon: FirebaseFirestore.QueryDocumentSnapshot) {
    switch (pokemon.data().type[0]) {
        case "bug": {
            return "C6D16E";
            break;
        }
        case "dark": {
            return "A29288";
            break;
        }
        case "dragon": {
            return "A27DFA";
            break;
        }
        case "electric": {
            return "FAE078";
            break;
        }
        case "fairy": {
            return "F4BDC9";
            break;
        }
        case "fighting": {
            return "D67873";
            break;
        }
        case "fire": {
            return "F5AC78";
            break;
        }
        case "flying": {
            return "C6B7F5";
            break;
        }
        case "ghost": {
            return "A292BC";
            break;
        }
        case "grass": {
            return "A7DB8D";
            break;
        }
        case "ground": {
            return "EBD69D";
            break;
        }
        case "ice": {
            return "BCE6E6";
            break;
        }
        case "normal": {
            return "C6C6A7";
            break;
        }
        case "poison": {
            return "C183C1";
            break;
        }
        case "psychic": {
            return "FA92B2";
            break;
        }
        case "rock": {
            return "D1C17D";
            break;
        }
        case "steel": {
            return "D1D1E0";
            break;
        }
        case "water": {
            return "9DB7F5";
            break;
        }
        default: {
            return "9DC1B7";
            break;
        }
    }
}

function _getImageUrl(pokemon: FirebaseFirestore.QueryDocumentSnapshot) {
    const entry: String = pokemon.data().entry.toString();
    const paddedEntry: String = entry.padStart(3, "0");
    return `https://assets.pokemon.com/assets/cms2/img/pokedex/full/${paddedEntry}.png`

}

function _getCryUrl(pokemon: FirebaseFirestore.QueryDocumentSnapshot) {
    var name: String = pokemon.data().name.toLowerCase();
    if (name == "nidoran (m)") {
        name = "nidoranm";
    } else if (name == "nidoran (f)") {
        name = "nidoranf";
    }
    return `https://play.pokemonshowdown.com/audio/cries/${name}.mp3`

}

app.get("/pokemon", async (req, res) => {

    try {
        const list: Array<any> = [];
        const pokemonCollection = db.collection('pokemon').orderBy("entry", "asc");
        await pokemonCollection.get().then((querySnap) => {
            querySnap.docs.forEach(async (pokemon) => {

                const color: String = _getColor(pokemon)
                const imageUrl: String = _getImageUrl(pokemon)
                const cryUrl: String = _getCryUrl(pokemon)

                list.push({
                    "entry": pokemon.data().entry,
                    "name": pokemon.data().name,
                    "description": pokemon.data().description,
                    "type": pokemon.data().type,
                    "url": imageUrl,
                    "cry": cryUrl,
                    "color": color
                })
            })
        })

        res.json({
            "pokemons": list
        })

    } catch {
        res.status(400).send({
            error: "Failed getting list of Pokemon"
        })
    }
})
