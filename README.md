This API returns some details of all 151 Pokemons.

To consume:
GET
`https://asia-northeast1-pokedex-p3030.cloudfunctions.net/api/v1/pokemon`


Here is a sample return from the API:

```{
    "pokemons": [
        {
            "entry": 1,
            "name": "Bulbasaur",
            "description": "Bulbasaur can be seen napping in bright sunlight. There is a seed on its back. By soaking up the sun's rays, the seed grows progressively larger.",
            "type": [
                "grass",
                "poison"
            ],
            "url": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/001.png",
            "cry": "https://play.pokemonshowdown.com/audio/cries/bulbasaur.mp3",
            "color": "A7DB8D"
        },
        {
            "entry": 2,
            "name": "Ivysaur",
            "description": "There is a bud on this Pokemon's back. To support its weight, Ivysaur's legs and trunk grow thick and strong. If it starts spending more time lying in the sunlight, it's a sign that the bud will bloom into a large flower soon.",
            "type": [
                "grass",
                "poison"
            ],
            "url": "https://assets.pokemon.com/assets/cms2/img/pokedex/full/002.png",
            "cry": "https://play.pokemonshowdown.com/audio/cries/ivysaur.mp3",
            "color": "A7DB8D"
        }
    ]
}
